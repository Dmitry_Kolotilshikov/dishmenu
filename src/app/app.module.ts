import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { OrdersComponent } from './components/orders/orders.component';
import { DishComponent } from './components/dish/dish.component';
import { ModalComponent } from './components/modal/modal.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { RoutingModule } from './routing.module';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { AboutPageComponent } from './pages/about/about.component';
import { LoginPageComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChannelComponent } from './pages/channels/channel/channel.component';
import { SwappingSquaresSpinnerModule } from 'angular-epic-spinners';
import { DateComponent } from './pages/channels/date/date.component';
import { DatePipe, TimePipe } from './pipes';
import { TvshowComponent } from './pages/channels/tvshow/tvshow.component';
import { AuthGuard, AuthService, DataService, LoadingService, LoginGuard, TimeService } from './services';
import { ColorDirective } from './directives/color.directive';
import { ClickDirective } from './directives/click.directive';


@NgModule({
  declarations: [
    AppComponent,
    DishComponent,
    OrdersComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    ChannelsPageComponent,
    AboutPageComponent,
    LoginPageComponent,
    ChannelComponent,
    DateComponent,
    DatePipe,
    TvshowComponent,
    TimePipe,
    ColorDirective,
    ClickDirective
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SwappingSquaresSpinnerModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    LoginGuard,
    DataService,
    LoadingService,
    TimeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
