import { Component, Input, OnInit, ViewChild, ElementRef, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})

export class DateComponent implements OnInit {

  static activeDate: string;

  @Input() date: string;
  @ViewChild('current', { static: false }) cur: ElementRef;
  @Output() dateEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      if (this.isActive) {
        const element: HTMLDivElement = this.cur.nativeElement;
        element.scrollIntoView({ behavior: 'smooth' });
      }
    }, 200);
  }

  public get isActive(): boolean {
    return this.date === DateComponent.activeDate;
  }

  public setActivedate(): void {
    DateComponent.activeDate = this.date;
    this.dateEvent.emit(this.date);
  }

}
